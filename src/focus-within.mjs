import focusable from './focusable.mjs'
import { selfAndParents } from './tree.mjs'

document.addEventListener('focusout', (e) => {
	for (const elem of Array.from(selfAndParents(e.target)).filter(focusable)) {
		elem.classList.remove('focus-within')
	}
})
document.addEventListener('focusin', (e) => {
	for (const elem of Array.from(selfAndParents(e.target)).filter(focusable)) {
		elem.classList.add('focus-within')
	}
})
