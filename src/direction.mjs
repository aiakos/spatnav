export function reverseDirection (direction) {
	switch (direction) {
		case 'up':
			return 'down'
		case 'down':
			return 'up'
		case 'left':
			return 'right'
		case 'right':
			return 'left'
	}
}

export function directionAxis (direction) {
	switch (direction) {
		case 'up':
		case 'down':
			return 'column'
		case 'left':
		case 'right':
			return 'row'
	}
}

export function directionLength (direction) {
	switch (direction) {
		case 'up':
		case 'left':
			return -1
		case 'down':
		case 'right':
			return +1
	}
}
