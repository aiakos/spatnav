export function* parents(start) {
	let elem = start
	while ((elem = elem.parentNode) && elem != document) {
		yield elem
	}
}

export function* selfAndParents(start) {
	yield start
	yield* parents(start)
}

export function* findUpper(start, selector) {
	// HTMLCollection is not iterable in current Edge (16)
	for (const el of Array.from(start.children)) {
		if (selector(el)) {
			yield el
		} else {
			yield* findUpper(el, selector)
		}
	}
}
