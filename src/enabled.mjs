export default function enabled ( element ) {
	if (element.matches(':disabled') || element.getAttribute('hidden') != null) {
		return false
	}

	switch (element.tagName.toLowerCase()) {
	case 'dialog':
		if (element.getAttribute('open') === null) {
			return false
		}
	}

	return (element.parentNode && element.parentNode.matches) ? enabled(element.parentNode) : true
}
