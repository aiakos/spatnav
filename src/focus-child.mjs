import focusable from './focusable.mjs'
import enabled from './enabled.mjs'
import { selfAndParents, findUpper } from './tree.mjs'

let lastFocus = null

document.addEventListener('focusout', (e) => {
	lastFocus = e.target
})

document.addEventListener('focusin', (e) => {
	// TODO Consider using jQuery delegated events for :focusable, as this implementation does not support bubbling.
	if (!e.defaultPrevented) {
		const focusableTarget = Array.from(selfAndParents(e.target)).filter(focusable).shift()
		const focusChild = focusableTarget.getAttribute('focus-child')
		if (focusChild != null) {
			if (lastFocus && enabled(lastFocus) && Array.from(findUpper(focusableTarget, x => x !== focusableTarget && x === lastFocus)).length) {
				// Don't change focus when we are already inside the activated container.
				lastFocus.focus()
			} else {
				const children = Array.from(findUpper(focusableTarget, focusable))
				const defaultChildren = children.filter(e => e.getAttribute('default-focus') != null)
				const autofocusableChildren = defaultChildren.length ? defaultChildren : children
				if (autofocusableChildren.length) {
					const child = (focusChild === 'last') ? autofocusableChildren.pop() : autofocusableChildren.shift()
					child.focus()
				}
			}
			e.preventDefault()
		}
	}
})
