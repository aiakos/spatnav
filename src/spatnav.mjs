import focusable from './focusable.mjs'
import enabled from './enabled.mjs'
import { parents, findUpper } from './tree.mjs'
import { reverseDirection, directionAxis, directionLength } from './direction.mjs'

function containerAxis (el) {
	return el.getAttribute('spatnav-direction') || (el.tagName.toLowerCase() === 'body' ? 'column' : 'row')
}

function doCancel (element) {
	const cancel = new CustomEvent('cancel', {})
	element.dispatchEvent(cancel)
	if (!cancel.defaultPrevented) {
		element.close()
	}
	return cancel
}

function escape () {
	const containers = Array.from(parents(document.activeElement)).filter(focusable)
	for (const container of containers) {
		if (container.close) {
			doCancel(container)
			return true
		}
	}
}

function moveFocus (direction) {
	const containers = Array.from(parents(document.activeElement)).filter(focusable)
	containers.push(document.body)
	for (const container of containers) {
		if (containerAxis(container) === directionAxis(direction)) {
			const focusableElements = Array.from(findUpper(container, focusable)).filter(enabled)
			const focus = focusableElements.filter(x => x.matches(':focus') || x.querySelector(':focus')).shift()
			const newfocus = focusableElements[focusableElements.indexOf(focus) + directionLength(direction)]
			if (newfocus) {
				newfocus.focus()
				return newfocus
			}
		}
		if (container.tagName.toLowerCase() === 'dialog') {
			return
		}
	}
}

document.addEventListener('keydown', (e) => {
	if (e.defaultPrevented) {
		return
	}

	let allowUp = true
	let allowDown = true
	let allowLeft = true
	let allowRight = true
	let allowClick = true

	switch (e.target.tagName.toLowerCase()) {
	case 'input':
		switch (e.target.type) {
		case 'button':
		case 'checkbox':
		case 'file':
		case 'image':
		case 'radio':
		case 'reset':
		case 'submit':
			break
		default:
			allowClick = false
			allowLeft = false
			allowRight = false

			if (e.target.selectionStart === e.target.selectionEnd) {
				if (e.target.selectionStart === 0) {
					allowLeft = true
				}
				if (e.target.selectionStart === e.target.value.length) {
					allowRight = true
				}
			}
		}
		break

	case 'textarea':
		allowClick = false
		allowUp = false
		allowDown = false
		allowLeft = false
		allowRight = false

		if (e.target.selectionStart === e.target.selectionEnd) {
			if (e.target.selectionStart === 0) {
				allowUp = true
				allowLeft = true
			}
			if (e.target.selectionStart === e.target.textLength) {
				allowDown = true
				allowRight = true
			}
		}
	}

	const modifier = e.shiftKey ? reverseDirection : (x) => x

	switch (e.key) {
	case 'ArrowUp':
	case 'Up': // Edge
		if (!allowUp) {
			return
		}
		if (moveFocus(modifier('up'))) {
			e.preventDefault()
		}
		break
	case 'ArrowDown':
	case 'Down': // Edge
		if (!allowDown) {
			return
		}
		if (moveFocus(modifier('down'))) {
			e.preventDefault()
		}
		break
	case 'ArrowLeft':
	case 'Left': // Edge
		if (!allowLeft) {
			return
		}
		if (moveFocus(modifier('left'))) {
			e.preventDefault()
		}
		break
	case 'ArrowRight':
	case 'Right': // Edge
		if (!allowRight) {
			return
		}
		if (moveFocus(modifier('right'))) {
			e.preventDefault()
		}
		break
	case 'Escape':
	case 'Esc': // Edge
		if (escape()) {
			e.preventDefault()
		}
		break
	case 'Enter':
	case ' ':
	case 'Spacebar': // Edge
		if (!allowClick) {
			return
		}
		e.preventDefault()
		e.target.click()
	}
})

document.addEventListener('close', (e) => {
	if (!e.target.spatnavListener) {
		e.target.spatnavListener = true
		e.target.addEventListener('close', (e) => {
			if (e.target.matches(':focus') || e.target.querySelector(':focus') || document.activeElement === document.body) {
				const parent = Array.from(parents(e.target)).filter(focusable).shift()
				parent.focus()
			}
		})
	}
}, {
	capture: true,
	passive: true,
})
