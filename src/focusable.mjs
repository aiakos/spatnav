export default function focusable ( element ) {
	// See https://www.w3.org/TR/html5/editing.html#focusable
	// Note that we do not consider disabled/hidden elements unfocusable.

	if (element === document) {
		return false
	}

	switch (element.tagName.toLowerCase()) {
		case 'input':
			if (element.getAttribute('type') === 'hidden') {
				return false
			}
		case 'button':
		case 'select':
		case 'textarea':
			return true

		case 'a':
		case 'link':
			if (element.getAttribute('href') != null) {
				return true
			}
			break

		case 'audio':
		case 'video':
			if (element.getAttribute('controls') != null) {
				return true
			}
			break

		case 'dialog':
			return true
	}

	return element.getAttribute('tabindex') != null
}
