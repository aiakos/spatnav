# spatnav - Drop-in keyboard spatial navigation engine

Spatnav adds keyboard navigation to your webapp without requiring you to write any JS code.

All top-level focusable elements are navigatable with Up/Down arrows. To use Left/Right arrows, you need to put the horizontally-placed elements in focusable (usually `tabindex="-1"`) containers with `spatnav-direction="row" focus-child`. You may choose the child to autofocus with `focus-child="first"` (default) and `focus-child="last"` on the container, and/or with `default-focus` attribute on the child.

Enter and Space keys trigger `click` on the focused element. "Though the click event type has its origins in pointer devices (e.g., a mouse), subsequent implementation enhancements have extended it beyond that association, and it can be considered a device-independent event type for element activation." - [the UI Events spec](https://www.w3.org/TR/DOM-Level-3-Events/#click)

Elements that provide `.close()` method can be closed by pressing Esc. This may be used to create menus:
```html
<div tabindex="-1" focus-child>
    <span tabindex="0">Menu 1</span>
    <div class="menu" hidden tabindex="-1" focus-child>
        <ul>
            <li><a href>Link 1.1</a></li>
            <li><a href>Link 1.2</a></li>
            <li><a href>Link 1.3</a></li>
        </ul>
    </div>
</div>

<script>
for (const el of document.querySelectorAll('.menu')) {
    el.close = function () {
        this.setAttribute('hidden', '')
        const closeEvent = new CustomEvent('close')
        this.dispatchEvent(closeEvent)
    }
}
</script>
```

HTML5 Dialogs are also supported (in fact the menu functionality is modelled after them).
